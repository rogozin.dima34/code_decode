from FileReaderInterface import FileReaderInterface


class Generate_File_Chunk:
    def __init__(self, file_path, buffer_size):
        self.file_path = file_path
        self.buffer_size = buffer_size
        self.file = None

    def __iter__(self):
        with open(self.file_path, 'r') as file:
            while True:
                chunk = file.read(self.buffer_size)
                if not chunk:
                    break
                yield chunk

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass


class MyFileReader(FileReaderInterface):
    def read_file(self, file_path, buffer_size):
        return Generate_File_Chunk(file_path, int(buffer_size))
