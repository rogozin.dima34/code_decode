from CoderInterface import CoderInterface

class MyCoder(CoderInterface):
    def run(self, coder_info, string_to_process):
        if coder_info == 'code':
            return self._code(string_to_process)
        if coder_info == 'decode':
            return self._decode(string_to_process)


    def _code(self, string_to_code):


        alphabet = "abcdefghijklmnopqrstuvwxyz"
        cipher = {alphabet[i]: alphabet[::-1][i] for i in range(len(alphabet))}
        result = "".join(cipher.get(symbol, symbol) for symbol in string_to_code)


        return result

    def _decode(self, string_to_code):



        return self._code(string_to_code)
